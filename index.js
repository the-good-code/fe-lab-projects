const express = require('express');
const morgan = require('morgan');
const app = express();

const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');


app.use(morgan("dev"))
app.use(express.json())

const dir = './data';

(async () => {
    let fileExists = await fsPromises.access(dir, fs.constants.F_OK)
        .then(() => true)
        .catch(() => false)

    if (!fileExists) {
        await fsPromises.mkdir(dir)
    }
})();

app.post('/api/files', async (req, res) => {
    let newFile = req.body;
    let fileContent = newFile.content;
    let fileName = newFile.filename
    const regEx = /(?:\.([^.]+))?$/;
    const extension = regEx.exec(fileName)[1];
    const allowextensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

    if (extension === undefined) {
        res.status(400).send({ message: 'Please specify file extension' })
        return;
    }
    if (!allowextensions.includes(extension)) {
        res.status(400).send({ message: `Only 'log', 'txt', 'json', 'yaml', 'xml', 'js' file extensions are accepted` })
        return;
    }

    if (typeof fileContent !== 'string') {
        res.status(400).send({ message: 'Please specify "content" parameter' })
        return;
    }

    try {
        await fsPromises.writeFile(`${dir}/${fileName}`, fileContent);
        res.status(200).send({ message: 'File created successfully' })
    } catch (err) {
        res.status(500).send({ message: 'Server error' })
    }

});


app.get('/api/files', async (req, res) => {

    try {
        const data = await fsPromises.readdir(dir);
        res.status(200).send({
            message: 'Success',
            files: data
        })
    } catch (err) {
        res.status(500).send({ message: 'Server error' })
    }

});


app.get(`/api/files/:filename`, async (req, res) => {
    const fileName = req.params.filename;
    const regEx = /(?:\.([^.]+))?$/;
    const extension = regEx.exec(fileName)[1];

    if (extension === undefined) {
        res.status(400).send({ message: 'Please specify file extension' })
        return;
    }

    try {
        const data = await fsPromises.readFile(`${dir}/${fileName}`, 'utf8');
        const info = await fsPromises.stat(`${dir}/${fileName}`);
        res.status(200).send({
            message: 'Success',
            filename: fileName,
            content: data,
            extension: extension,
            uploadedDate: info.birthtime
        })
    } catch (err) {

        if (err.toString().includes(`No file with ${filename} filename found`)) {
            res.status(400).send({ message: 'No such file' })
            return;
        }

        res.status(500).send({ message: 'Server error' })
    }
});


app.listen(8080);
